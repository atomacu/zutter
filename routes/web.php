<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['role']], function(){
    
    
    Route::get('/', function () {
        return view('welcome');
    })->name("welcome");

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/stocs', 'Admin\PagesController@stocs')->name('stocs');
    Route::resource('admin', 'Admin\AdminController');

    Route::resource('admin-stoc', 'Admin\StocController');
    Route::get('/admin-stoc/get-by-ware-id/{id}', 'Admin\StocController@getByWareId')->name('admin-stoc.getByWareId');
    Route::get('/admin-stoc/get-by-ware-and-size-id/{ware_id}/{size_id}', 'Admin\StocController@getByWareAndSizeId')->name('admin-stoc.getByWareAndSizeId');

    Route::resource('admin-stoc-colors', 'Admin\ColorController');
    Route::resource('admin-stoc-sizes', 'Admin\SizeController');
    Route::resource('admin-stoc-wares', 'Admin\WareController');
    Route::resource('admin-orders', 'Admin\OrderController');
    Route::post('/get-stock', 'Admin\OrderController@getStock')->name('getStock');
    Route::post('/delete-order-item', 'Admin\OrderController@deleteOrderItem')->name('deleteOrderItem');
    Route::post('/edit-order-item', 'Admin\OrderController@editOrderItem')->name('editOrderItem');


    
    Route::resource('admin-order-data', 'Admin\OrderDataController');
    Route::resource('admin-item', 'Admin\ItemController');
    Route::resource('admin-settings', 'Admin\SettingsController');
    Route::get('admin-settings-get-roles', 'Admin\SettingsController@getRoles')->name('admin-settings.getRoles');
   
    
    
    
    
});


