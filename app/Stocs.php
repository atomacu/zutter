<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stocs extends Model
{
    protected $fillable = [
        "wares_id", "colors_id", "sizes_id", "quantitaty"
    ];

    public function color()
    {
        return $this->belongsTo('App\Color',"colors_id");
    }

    public function size()
    {
        return $this->belongsTo('App\Size',"sizes_id");
    }

    public function ware()
    {
        return $this->belongsTo('App\Ware',"wares_id");
    }
}


