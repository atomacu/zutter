<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "name", "address", "phone", "delivery_type_id","delivery_date",
    ];

    public function delivery_type(){
        return $this->belongsTo('App\DeliveryType',"delivery_type_id");
    }
    public function items(){
        return $this->hasMany('App\Item',"order_id")->with("stock");
    }
}
