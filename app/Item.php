<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        "order_id", "stocs_id", "quantitaty",'description', "img_base"
    ];

    public function stock(){
        return $this->belongsTo("App\Stocs", "stocs_id")->with("color")->with("size")->with("ware");
    }
}
