<?php

namespace App\Http\Controllers\Admin;

use App\Stocs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return json_encode(Stocs::with("color")->with("size")->with("ware")->get());
    }

    public function getByWareId($id)
    {
        return json_encode(Stocs::where("wares_id",$id)->with("color")->with("size")->with("ware")->get());
    }

    public function getByWareAndSizeId($wareId, $sizeId)
    {
        return json_encode(Stocs::where("wares_id",$wareId)->where('sizes_id',$sizeId)->with("color")->with("size")->with("ware")->get());
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stoc = Stocs::where('wares_id',$request['selectedWare'])
        ->where('sizes_id',$request['selectedSize'])
        ->where('colors_id',$request['selectedColor'])
        ->first();
        
        if(!$stoc){
            if($request['quantitaty']>0){
                Stocs::create([
                    "wares_id"=>$request['selectedWare'],
                    "sizes_id"=>$request['selectedSize'],
                    "colors_id"=>$request['selectedColor'],
                    "quantitaty"=>$request['quantitaty']
                ])->id;
            }else{
                return 400;
            }
        }else{
            if($stoc->quantitaty<0){
                $stoc->quantitaty = 0;
                $stoc->save();
            }
            if(($stoc->quantitaty+$request['quantitaty'])>=0){
                $stoc->update([
                    "quantitaty"=>($stoc->quantitaty+$request['quantitaty'])
                ]);
                $stoc->save();
            }else{
                return 400;
            }



        }
        return 200;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
