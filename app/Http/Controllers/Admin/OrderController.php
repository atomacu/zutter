<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Item;
use App\Stocs;
use App\Order;
use App\DeliveryType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date=date_create($request['deliveryDate']);
        $date = date_format($date,"Y-m-d");

        $order = Order::create([
            "name" => $request["name"],
            "address" => $request["address"],
            "phone" => $request["phone"],
            "delivery_date" => $date,
            "delivery_type_id" => $request["deliveryId"]
        ]);
        foreach($request['items'] as $item){
            
            $stock = Stocs::where('wares_id',$item['wareId'])
            ->where("colors_id",$item['colorId'])
            ->where("sizes_id",$item['sizeId'])
            ->first();

            Item::create([
                "order_id"=>$order->id,
                "stocs_id"=>$stock->id,
                "quantitaty"=>$item['quantitaty'],
                "description"=>$item['descriptionOrder'],
                "img_base" => $item['imgeBase']
            ]);
        }

        return route("admin-orders.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with("delivery_type")->with('items')->find($id);
        return view("admin.order.show",compact("order"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function daysUntilDeadline(){
        
    }


    public function getStock(Request $request){

     $whatInStock = Stocs::where('wares_id',$request['wareId'])->where('sizes_id', $request['sizeId'])->where('colors_id', $request['colorId'])->first();

   
     if ($request['quantitaty'] > $whatInStock['quantitaty']){
        return json_encode(false);
     } else {
         return json_encode(true);
     }
    }

    public function deleteOrderItem(Request $request){
       Order::where("id", $request['orderId'])->delete();
      Item::where("order_id", $request['orderId'])->delete();
   

    }

    public function editOrderItem(Request $request){

       return Order::where("id", $request['currentEditId'])->update([
            "name"=> $request['currentEditName'],
            "address"=>$request['currentEditAddress'],
            "phone"=>$request['currentEditPhone'],
           
        ]);
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
