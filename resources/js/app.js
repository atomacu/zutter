require("./bootstrap");

window.Vue = require("vue");
import BootstrapVue from "bootstrap-vue";

Vue.use(BootstrapVue);
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component(
    "stoc-config",
    require("./components/admin/stoc/stoc-config.vue").default
);
Vue.component(
    "order-index",
    require("./components/admin/order/index.vue").default
);
Vue.component(
    "order-create",
    require("./components/admin/order/create.vue").default
);
Vue.component(
    "show-order",
    require("./components/admin/order/show.vue").default
);
Vue.component(
    "order-settings-component",
    require("./components/admin/settings/order.vue").default
);
Vue.component(
    "profile-settings-component",
    require("./components/admin/settings/profile.vue").default
);
Vue.component(
    "set-role-access-settings-component",
    require("./components/admin/settings/roles-access.vue").default
);
Vue.component(
    "admin-side-meniu",
    require("./components/layouts/admin.vue").default
);



const app = new Vue({
    el: "#app"
});
