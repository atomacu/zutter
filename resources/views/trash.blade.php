v-text="'Lipseste'" @blur="tdBlurEdit" @focus="getRowIndex(index, 'address')"
@keydown.enter="$event.target.blur()" contenteditable="true"
tdBlurEdit: function (evt) {
let src = evt.target.innerHTML;
this.orders[this.rowIndex].name = src;

let json = {
id: this.orders[this.rowIndex].id,
col: this.modifiedCol,
val: this.orders[this.rowIndex].name
};
console.log(json);
},
getRowIndex: function (index, col) {
this.rowIndex = index;
this.modifiedCol = col;
}