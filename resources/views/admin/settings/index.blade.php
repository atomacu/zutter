@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-3">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link" id="v-pills-profile-settings-tab" data-toggle="pill" href="#v-pills-profile-settings" role="tab" aria-controls="v-pills-profile-settings" aria-selected="true">Profil</a>
        <a class="nav-link active" id="v-pills-order-settings-tab" data-toggle="pill" href="#v-pills-order-settings" role="tab" aria-controls="v-pills-order-settings" aria-selected="false">Setările comenzii</a>
        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Roluri si accesibilitate</a>
        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
      </div>
    </div>
    <div class="col-9">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane fade" id="v-pills-profile-settings" role="tabpanel" aria-labelledby="v-pills-profile-settings-tab">
            <profile-settings-component></profile-settings-component>
        </div>
        <div class="tab-pane fade show active" id="v-pills-order-settings" role="tabpanel" aria-labelledby="v-pills-order-settings-tab">
            <order-settings-component></order-settings-component>
        </div>
        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
          <set-role-access-settings-component></set-role-access-settings-component>
        </div>
        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
      </div>
    </div>
  </div>
@endsection
