<?php

use App\Role;
use App\UserRole;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {       
        //                      $name,$description,$defaultAccess=0,$defaultPage,$setDefault=0
        $roleId = Role::store('Admin',"Poate face tot ce vrea.",1,'admin',0);
        Role::store('Moderator',"Poate face tot ce vrea.",1,'Moderator',0);
        $roleId1 = Role::store('Chat manager',"Nu poate face chiar tot ce vrea.",1,'chat_manager',1);
        Role::store('Designer',"Nu poate face tot ce vrea.",0,'designer',0);
        Role::store('Printing',"Nu poate face tot ce vrea.",0,'printing',0);
        Role::store('Livrare',"Nu poate face tot ce vrea.",0,'livrare',0);

        $userId = DB::table('users')->insertGetId([
            'name' => "Tomacu Alexandru",
            'email' => "atomacu@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);

        $userId2 = DB::table('users')->insertGetId([
            'name' => "Xenia Gaina",
            'email' => "xenia@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);

        $userId1 = DB::table('users')->insertGetId([
            'name' => "Tomacu Alexandru",
            'email' => "moderator@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);

        DB::table('delivery_types')->insert([
            'name' => "Curier",
            'need_address'=>1
        ]);
        DB::table('delivery_types')->insert([
            'name' => "Prin poștă",
            'need_address'=>1
        ]);
        DB::table('delivery_types')->insert([
            'name' => "Oficiu"
        ]);


        DB::table('wares')->insert([
            'name' => "Tricou"
        ]);

        DB::table('wares')->insert([
            'name' => "Hanorac"
        ]);
        DB::table('wares')->insert([
            'name' => 'Chipiu'
        ]);

        DB::table('colors')->insert([
            'name' => "Albastru"
        ]);

        DB::table('colors')->insert([
            'name' => "Negru"
        ]);
        DB::table('colors')->insert([
            'name' => "Rosu"
        ]);

        DB::table('sizes')->insert([
            'name' => "XL"
        ]);

        DB::table('sizes')->insert([
            'name' => "L"
        ]);
        DB::table('sizes')->insert([
            'name' => "M"
        ]);
        DB::table('sizes')->insert([
            'name' => "S"
        ]);

        DB::table('stocs')->insert([
            "wares_id"=>"1",
            "colors_id"=>"2",
            "sizes_id"=>"1",
            "quantitaty"=>"100"
        ]);
        DB::table('stocs')->insert([
            "wares_id"=>"3",
            "colors_id"=>"3",
            "sizes_id"=>"3",
            "quantitaty"=>"4"
        ]);

        DB::table('stocs')->insert([
            "wares_id"=>"1",
            "colors_id"=>"1",
            "sizes_id"=>"1",
            "quantitaty"=>"100"
        ]);



        DB::table('stocs')->insert([
            "wares_id"=>"2",
            "colors_id"=>"1",
            "sizes_id"=>"2",
            "quantitaty"=>"100"
        ]);

        DB::table('stocs')->insert([
            "wares_id"=>"2",
            "colors_id"=>"2",
            "sizes_id"=>"2",
            "quantitaty"=>"100"
        ]);
        
        UserRole::store($userId,$roleId);
        UserRole::store($userId2,$roleId);

        UserRole::store($userId1,$roleId1);

      
    }
}
