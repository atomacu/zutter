<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('wares_id')->unsigned();
            $table->foreign('wares_id')->references('id')->on('wares')->onDelete('cascade');
            $table->bigInteger('colors_id')->unsigned();
            $table->foreign('colors_id')->references('id')->on('colors')->onDelete('cascade');
            $table->bigInteger('sizes_id')->unsigned();
            $table->foreign('sizes_id')->references('id')->on('sizes')->onDelete('cascade');
            $table->integer('quantitaty'); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocs');
    }
}
